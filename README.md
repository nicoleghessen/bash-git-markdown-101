 **Bash, Git, Bitbucket and Markdown Intro**

### Bash 
is a Linux language to interact with operating systems, we do this via the terminal. 

##### Path to Working Directory
``` $ pwd ```

##### Move back to previous directory 
``` $ cd.. ```

##### Change Directory
```$ cd desktop ```

##### Make Directory
```$ mkdir ```

#### See list of folders in directory 
``` $ ls ```

##### To see all folders or files including hidden 
``` $ ls -a ```

##### Remove Directory
```$ rm -rf <name> ```

##### To create a file 
``` $ touch <name> ```





##### Git

Git is version control

## Main Git Commands

###### to use git or start git 
``` $ git init ```

###### Check Git timeline
``` $ git status ```

###### To add/update file(s)
``` $ git add <nameoffile> ```

###### To apply chnages to file(s)
``` $ git commit -m "<whatyoudid>" ```



##### Bitbucket 
``` Bitbucket is an online location for your code to live and is collaborative. ```

``` Add a remote ```
`git remote add origin <origin name>`

`git push origin master` - uploads document to Bitbucket

Check Bitbucket has the README document uploaded 

Check history of committs to see what has been edited and when. 







``` Push a remote repo on Bitbucket```